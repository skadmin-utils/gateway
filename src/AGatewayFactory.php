<?php

declare(strict_types=1);

namespace SkadminUtils\Gateway;

use SkadminUtils\Gateway\Transaction\AGatewayTransaction;

abstract class AGatewayFactory
{
    abstract public function process(AGatewayTransaction $transaction, string $code): mixed;
}
