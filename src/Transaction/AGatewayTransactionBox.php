<?php

declare(strict_types=1);

namespace SkadminUtils\Gateway;

abstract class AGatewayTransactionBox
{
    private mixed $object;

    public function __construct(mixed $object)
    {
        $this->object = $object;
    }

    public function getObject(): mixed
    {
        return $this->object;
    }
}
