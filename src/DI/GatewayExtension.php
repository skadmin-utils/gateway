<?php

declare(strict_types=1);

namespace SkadminUtils\Gateway\DI;

use Nette\DI\CompilerExtension;
use Nette\DI\Definitions\Statement;
use Nette\Schema\Expect;
use Nette\Schema\Schema;
use SkadminUtils\Gateway\GatewayCallback;
use SkadminUtils\Gateway\GatewayFactory;

use function assert;
use function class_exists;
use function is_callable;

class GatewayExtension extends CompilerExtension
{
    public function getConfigSchema(): Schema
    {
        return Expect::structure([
            'callbacks'      => Expect::arrayOf(Expect::arrayOf(Expect::type(Statement::class))),
            'availableCodes' => Expect::arrayOf(Expect::string()),
        ]);
    }

    public function loadConfiguration(): void
    {
        parent::loadConfiguration();

        $callbacks = [];

        foreach ($this->config->callbacks as $type => $callbackArrays) {
            foreach ($callbackArrays as $gatewayCallbackStatement) {
                assert($gatewayCallbackStatement instanceof Statement);
                [$class, $gatewayFactory, $callback] = $gatewayCallbackStatement->arguments;
                if ($gatewayCallbackStatement->getEntity() !== GatewayCallback::class || ! class_exists($class) || ! is_callable($callback)) {
                    continue;
                }

                $callbacks[$type][$class] = $gatewayCallbackStatement;
            }
        }

        $builder = $this->getContainerBuilder();
        $builder->addDefinition($this->prefix('gatewayFactory'))
            ->setClass(GatewayFactory::class)
            ->setArguments([
                $builder->getDefinition('http.response'),
                $builder->getDefinition('session.session'),
                $callbacks,
                $this->config->availableCodes,
            ]);
    }
}
