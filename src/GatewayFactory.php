<?php

declare(strict_types=1);

namespace SkadminUtils\Gateway;

use Nette\Http\Response;
use Nette\Http\Session;
use Nette\Http\SessionSection;
use Skadmin\Payment\Doctrine\Payment\Payment;

use function array_flip;
use function call_user_func_array;
use function is_a;

class GatewayFactory
{
    public const  CODE             = 'gateway';
    private const CODE_BARION      = 'gateway-barion';
    private const CODE_BARION_BANK = 'gateway-barion-bank-transfer';

    private Response       $response;
    private SessionSection $session;
    /** @var array<array<GatewayCallback>> */
    private array $callbacks;
    /** @var array<string> */
    private array $availableCodes;

    /**
     * @param array<array<GatewayCallback>> $callbacks
     * @param array<string>                 $availableCodes
     */
    public function __construct(Response $response, Session $session, array $callbacks, array $availableCodes)
    {
        $this->response       = $response;
        $this->session        = $session->getSection(self::CODE);
        $this->callbacks      = $callbacks;
        $this->availableCodes = array_flip($availableCodes);
    }

    private function createTransaction(Payment $payment, mixed $object): mixed
    {
        $gatewayCallback = null;

        if (isset($this->callbacks[$payment->getCode()])) {
            $gatewayCallback = $this->processCallback($this->callbacks[$payment->getCode()], $object);
        }

        if (! $gatewayCallback instanceof GatewayCallback) {
            return null;
        }

        if ($object instanceof AGatewayTransactionBox) {
            $object = $object->getObject();
        }

        $transaction = call_user_func_array($gatewayCallback->getCallback(), [$object]);

        return $gatewayCallback->getGatewayFactory()->process($transaction, $payment->getCode());
    }

    /**
     * @param array<GatewayCallback> $gatewayCallbacks
     */
    private function processCallback(array $gatewayCallbacks, mixed $object): ?GatewayCallback
    {
        foreach ($gatewayCallbacks as $gatewayCallback) {
            if (is_a($object, $gatewayCallback->getSourceClass())) {
                return $gatewayCallback;
            }
        }

        return null;
    }

    public function redirectToGateway(Payment $payment, mixed $object, ?string $backLink = null): void
    {
        $gatewayResponse = $this->createTransaction($payment, $object);

        switch ($payment->getCode()) {
            case self::CODE_BARION:
            case self::CODE_BARION_BANK:
                if ($backLink !== null) {
                    $this->session->{$gatewayResponse->PaymentRequestId} = $backLink;
                }

                $this->response->redirect($gatewayResponse->PaymentRedirectUrl);
                break;
        }
    }

    /**
     * @return array<string>
     */
    public function getAvailableCodes(): array
    {
        return $this->availableCodes;
    }
}
