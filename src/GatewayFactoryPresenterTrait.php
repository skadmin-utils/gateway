<?php

declare(strict_types=1);

namespace SkadminUtils\Gateway;

//phpcs:disable SlevomatCodingStandard.Classes.SuperfluousTraitNaming.SuperfluousSuffix

use Nette\Application\UI\ITemplate;

trait GatewayFactoryPresenterTrait
{
    public GatewayFactory $gatewayFactory;

    public function injectGatewayFactory(GatewayFactory $gatewayFactory): void
    {
        $this->gatewayFactory = $gatewayFactory;
    }

    public function createTemplate(): ITemplate
    {
        $template                 = parent::createTemplate();
        $template->gatewayFactory = $this->gatewayFactory;

        return $template;
    }
}

// phpcs:enable
