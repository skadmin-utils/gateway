<?php

declare(strict_types=1);

namespace SkadminUtils\Gateway;

class GatewayCallback
{
    private string          $sourceClass;
    private AGatewayFactory $gatewayFactory;

    /** @var callable */
    private $callback;

    public function __construct(string $sourceClass, AGatewayFactory $gatewayFactory, callable $callback)
    {
        $this->sourceClass    = $sourceClass;
        $this->gatewayFactory = $gatewayFactory;
        $this->callback       = $callback;
    }

    public function getSourceClass(): string
    {
        return $this->sourceClass;
    }

    public function getGatewayFactory(): AGatewayFactory
    {
        return $this->gatewayFactory;
    }

    public function getCallback(): callable
    {
        return $this->callback;
    }
}
